package m3_clases_abstractas.figuras;

public class Rectagle extends Figura{
    
    private double amplada;
    private double altura;

    public Rectagle(double amplada, double altura, String nom) {
        super(nom);
        this.amplada = amplada;
        this.altura = altura;
    }
    
    public double parametre(){
        return 0;
    }
    
    public double area(){
        return 0;
    }

    
    //getters
    public double getAmplada() {
        return amplada;
    }

    public double getAltura() {
        return altura;
    }

    
    //setters
    public void setAmplada(double amplada) {
        this.amplada = amplada;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Rectagle{" + "amplada=" + amplada + ", altura=" + altura + '}';
    }
    
    
    
    
}
