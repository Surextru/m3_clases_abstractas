package m3_clases_abstractas.figuras;

public class Cercle extends Figura{
    private double radi;

    public Cercle(double radi, String nom) {
        super(nom);
        this.radi = radi;
    }
    
    @Override
    public double parametre(){
        return 0;
    }
    @Override
    public double area(){
        return 0;
    }

    public double getRadi() {
        return radi;
    }

    public void setRadi(double radi) {
        this.radi = radi;
    }

    @Override
    public String toString() {
        return "Cercle{" + "radi=" + radi + '}';
    }
    
    
}
