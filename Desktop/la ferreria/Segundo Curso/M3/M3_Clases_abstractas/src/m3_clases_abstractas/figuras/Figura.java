package m3_clases_abstractas.figuras;

public abstract class Figura {
    private String nom;

    public Figura(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
    
    public abstract double parametre();
    public abstract double area();
}
