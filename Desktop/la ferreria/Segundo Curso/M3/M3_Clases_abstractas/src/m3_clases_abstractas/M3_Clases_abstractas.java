
package m3_clases_abstractas;

import m3_clases_abstractas.figuras.Cercle;
import m3_clases_abstractas.figuras.Figura;
import m3_clases_abstractas.figuras.Rectagle;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jaime
 */
public class M3_Clases_abstractas {

    public static void main(String[] args) {
        
        
        //1.-
        List<Figura> figures = new ArrayList();
        
        
        //2.-
        Rectagle rect = new Rectagle(30, 30, "rectangulo");
        Cercle circulo = new Cercle(5, "redondeta");
        
        figures.add(rect);
        figures.add(circulo);
        
        //3.-
        
        Figura cir_fig = new Cercle(3, "redonda_fig");
        figures.add(cir_fig);
        
        //4.-Ni idea
        
        
        //5.-
        for(Figura f : figures){
            System.out.println(f);
        }
        
        
        
        
    }

}
